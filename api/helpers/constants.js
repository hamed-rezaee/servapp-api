module.exports = Object.freeze({
  BASE_URL: "/api/v1",
  PORT: process.env.PORT || 3000,
  JWT_PRIVATE_KEY: process.env.JWT_PRIVATE_KEY || "jwtprivatekey",

  SMS_API_KEY: "59475471577059387A59794D72466F483330784D6747336C4E51393861536351",

  SMS_TEMPLATE_VALIDATION_CODE: "ServappValidationCode",
  SMS_TEMPLATE_FORGOT_ACCOUNT: "ServappForgotAccount",

  SMS_IS_AVAILABLE: process.env.SMS_IS_AVAILABLE_KEY || false,

  DATABASE_URL: "mongodb://localhost/servapp_db",

  FILE_ENCODING: "base64",

  SALT_SIZE: 10
});
