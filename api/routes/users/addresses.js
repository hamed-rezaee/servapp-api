const express = require("express");
const httpStatus = require("http-status");

const Errors = require("../../helpers/errors");

const { Profile } = require("../../models/profile");
const { Address, validateSave } = require("../../models/address");

const router = express.Router();

router.post("/get", async (request, response) => {
  try {
    const id = request.body._id;
    let addresses;

    if (id) {
      addresses = await Address.findOne({ _id: id, deleted: false })
        .select("_id title detail range latitude longitude")
        .populate({
          path: "profile",
          select: "_id"
        })
        .populate({ path: "staticMap", select: "_id" });
    } else {
      addresses = await Address.find({ deleted: false })
        .select("_id title detail range latitude longitude")
        .populate({
          path: "profile",
          select: "_id"
        })
        .populate({ path: "staticMap", select: "_id" });
    }

    response.status(httpStatus.OK).json(addresses);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/save", async (request, response) => {
  try {
    const id = request.body._id;
    const { error } = validateSave(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let address = await Address.findOne({ _id: id, deleted: false });
    const isUpdate = address;

    if (isUpdate) {
      address.title = request.body.title;
      address.detail = request.body.detail;
      address.range = request.body.range;
      address.latitude = request.body.latitude;
      address.longitude = request.body.longitude;
    } else {
      address = new Address({
        title: request.body.title,
        detail: request.body.detail,
        range: request.body.range,
        latitude: request.body.latitude,
        longitude: request.body.longitude,
        profile: request.body.profileId,
        staticMap: request.body.staticMapId
      });
    }

    address = await address.save();

    if (!isUpdate) {
      const profile = await Profile.findById(request.body.profileId);

      profile.addresses.push(address._id);

      await profile.save();
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/delete", async (request, response) => {
  try {
    const id = request.body._id;
    const address = await Address.findOne({ _id: id, deleted: false });

    if (address) {
      address.deleted = true;

      await address.save();
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

module.exports = router;
