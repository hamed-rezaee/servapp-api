const express = require("express");
const httpStatus = require("http-status");

const Errors = require("../../helpers/errors");

const { User } = require("../../models/user");
const { Profile, validateSave } = require("../../models/profile");

const router = express.Router();

router.post("/get", async (request, response) => {
  try {
    const id = request.body._id;
    let profiles;

    if (id) {
      profiles = await Profile.findOne({ _id: id, deleted: false })
        .select("_id firstName lastName nationalCode score")
        .populate("avatar", "_id fileName contentType uploadDate")
        .populate("addresses", "_id  title detail range latitude longitude");
    } else {
      profiles = await Profile.find({ deleted: false })
        .select("_id firstName lastName nationalCode score")
        .populate("avatar", "_id fileName contentType uploadDate")
        .populate("addresses", "_id  title detail range latitude longitude");
    }

    response.status(httpStatus.OK).json(profiles);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/save", async (request, response) => {
  try {
    const id = request.body._id;
    const { error } = validateSave(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let profile = await Profile.findOne({ _id: id, deleted: false });
    const isUpdate = profile;

    if (isUpdate) {
      profile.firstName = request.body.firstName;
      profile.lastName = request.body.lastName;
      profile.nationalCode = request.body.nationalCode;
      profile.avatar = request.body.avatarId;
    } else {
      profile = new Profile({
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        nationalCode: request.body.nationalCode,
        avatar: request.body.avatarId
      });
    }

    profile = await profile.save();

    if (!isUpdate) {
      const user = await User.findById(request.body.userId);
      user.profile = profile._id;

      await user.save();
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/delete", async (request, response) => {
  try {
    const id = request.body._id;
    const profile = await Profile.findOne({ _id: id, deleted: false });

    if (profile) {
      profile.deleted = true;

      await profile.save();
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

module.exports = router;
