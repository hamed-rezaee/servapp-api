const bcrypt = require("bcrypt");
const express = require("express");
const shortId = require("shortid");
const Kavenegar = require("kavenegar");
const httpStatus = require("http-status");

const Constants = require("../../helpers/constants");
const Errors = require("../../helpers/errors");

const {
  User,
  validateRgister,
  validateLogin,
  validatePhoneNo,
  validateActivationCode
} = require("../../models/user");

const router = express.Router();

const kavenegarApi = Kavenegar.KavenegarApi({
  apikey: Constants.SMS_API_KEY
});

router.post("/get", async (request, response) => {
  try {
    const id = request.body._id;
    let users;

    if (id) {
      users = await User.findOne({ _id: id, deleted: false })
        .select("_id username phoneNo code")
        .populate("presenter", "_id username")
        .populate({
          path: "profile",
          select: "_id firstName lastName nationalCode score",
          populate: [
            {
              path: "avatar",
              select: "_id fileName contentType content uploadDate"
            },
            {
              path: "addresses",
              select: "_id title detail range latitude longitude"
            }
          ]
        });
    } else {
      users = await User.find({ deleted: false })
        .select("_id username phoneNo code")
        .populate("presenter", "_id username")
        .populate({
          path: "profile",
          select: "_id firstName lastName nationalCode score",
          populate: [
            {
              path: "avatar",
              select: "_id fileName contentType uploadDate"
            },
            {
              path: "addresses",
              select: "_id title detail range latitude longitude"
            }
          ]
        });
    }

    response.status(httpStatus.OK).json(users);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/register", async (request, response) => {
  try {
    const { error } = validateRgister(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.detail.message });
    }

    let user = await User.findOne({
      $or: [{ username: request.body.username }, { phoneNo: request.body.phoneNo }],
      deleted: false
    });

    if (user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_USER_ALREADY_REGISTERED });
    }

    const presenter = await User.findOne({
      code: request.body.presenterCode,
      deleted: false
    }).select("presenter");

    const activationCode = shortId.generate();

    user = new User({
      username: request.body.username,
      phoneNo: request.body.phoneNo,
      password: await bcrypt.hash(request.body.password, Constants.SALT_SIZE),
      code: shortId.generate(),
      activationCode: activationCode,
      presenter: presenter,
      profile: null,
      active: false
    });

    user = await user.save();

    response.status(httpStatus.OK).json(user);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/login", async (request, response) => {
  try {
    const { error } = validateLogin(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({
      username: request.body.username,
      deleted: false
    })
      .select("_id password username phoneNo code")
      .populate("presenter", "_id username")
      .populate({
        path: "profile",
        select: "_id firstName lastName nationalCode score",
        populate: {
          path: "avatar",
          select: "_id fileName contentType uploadDate"
        }
      });

    if (!user) {
      return response
        .status(httpStatus.UNAUTHORIZED)
        .json({ code: Errors.ERROR_INVALID_USER_OR_PASSWORD });
    }

    const authorized = await bcrypt.compare(request.body.password, user.password);

    if (!authorized) {
      return response
        .status(httpStatus.UNAUTHORIZED)
        .json({ code: Errors.ERROR_INVALID_USER_OR_PASSWORD });
    }

    user.token = await user.generateAuthToken();

    response.status(httpStatus.OK).json(user);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/activationCode", async (request, response) => {
  try {
    const { error } = validatePhoneNo(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({
      phoneNo: request.body.phoneNo,
      deleted: false
    });

    if (!user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_ACTIVATION_CODE });
    }

    if (user.active) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_USER_ALREADY_ACTIVATED });
    }

    if (Constants.SMS_IS_AVAILABLE) {
      await kavenegarApi.VerifyLookup({
        receptor: user.phoneNo,
        token: user.activationCode,
        template: Constants.SMS_TEMPLATE_VALIDATION_CODE
      });
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/sendAccountInformation", async (request, response) => {
  try {
    const { error } = validatePhoneNo(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({
      phoneNo: request.body.phoneNo,
      deleted: false
    });

    if (!user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_ACTIVATION_CODE });
    }

    const password = shortId.generate();

    user.password = await bcrypt.hash(password, Constants.SALT_SIZE);
    user.active = true;

    user = await user.save();

    if (Constants.SMS_IS_AVAILABLE) {
      const result = await kavenegarApi.VerifyLookup({
        receptor: user.phoneNo,
        token: user.username,
        token2: password,
        template: Constants.SMS_TEMPLATE_FORGOT_ACCOUNT
      });
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/activate", async (request, response) => {
  try {
    const { error } = validateActivationCode(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({
      activationCode: request.body.activationCode,
      deleted: false
    });

    if (!user) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_ACTIVATION_CODE });
    }

    user.active = true;

    user = await user.save();

    user.presenter = null;
    user.profile = null;
    user.token = await user.generateAuthToken();

    response.status(httpStatus.OK).json(user);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/validatePhoneNo", async (request, response) => {
  try {
    const { error } = validatePhoneNo(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    let user = await User.findOne({
      phoneNo: request.body.phoneNo,
      deleted: false
    });

    if (!user) {
      return response.status(httpStatus.BAD_REQUEST).json({ code: Errors.ERROR_INVALID_PHONE_NO });
    }

    return response.sendStatus(httpStatus.OK);
  } catch (error) {
    response0938
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/delete", async (request, response) => {
  try {
    const id = request.body._id;
    const user = await User.findOne({ _id: id, deleted: false });

    if (user) {
      user.deleted = true;

      await user.save();
    }

    response.sendStatus(httpStatus.OK);
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

module.exports = router;
