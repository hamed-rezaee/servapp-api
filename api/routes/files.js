const fs = require("fs");
const multer = require("multer");
const express = require("express");
const httpStatus = require("http-status");

const Constants = require("../helpers/constants");
const Errors = require("../helpers/errors");

const { File, validateDownloadRequest } = require("../models/file");

const router = express.Router();

const storage = multer.diskStorage({
  destination: function(request, file, cb) {
    cb(null, "uploads");
  },
  filename: function(request, file, cb) {
    cb(null, `${Date.now()}-${file.originalname}`);
  }
});

const uploader = multer({ storage: storage });

router.post("/download", async (request, response) => {
  try {
    const id = request.body._id;
    const { error } = validateDownloadRequest(request.body);

    if (error) {
      return response
        .status(httpStatus.BAD_REQUEST)
        .json({ code: Errors.ERROR_INVALID_DATA, message: error.message });
    }

    const file = await File.findOne({ _id: id, deleted: false });

    if (!file) {
      return response.status(httpStatus.BAD_REQUEST).json({ code: Errors.ERROR_INVALID_DATA });
    }

    return response
      .status(httpStatus.OK)
      .contentType(file.contentType)
      .send({ content: file.content, id: file._id });
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

router.post("/upload/:id*?", uploader.single("file"), async (request, response) => {
  try {
    const id = request.params.id;
    const image = fs.readFileSync(request.file.path);
    const encodeImage = image.toString(Constants.FILE_ENCODING);
    let file;

    if (id) {
      file = await File.findOne({ _id: id, deleted: false });

      file.fileName = request.file.originalname;
      file.contentType = request.file.mimetype;
      file.content = new Buffer.from(encodeImage, Constants.FILE_ENCODING);
      file.uploadDate = Date.now();
    } else {
      file = new File({
        fileName: request.file.originalname,
        contentType: request.file.mimetype,
        content: new Buffer.from(encodeImage, Constants.FILE_ENCODING),
        uploadDate: Date.now()
      });
    }

    file = await file.save();

    fs.unlinkSync(request.file.path);

    return response.status(httpStatus.OK).json({ _id: file._id });
  } catch (error) {
    response
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ code: Errors.ERROR_SERVER_ERROR, message: error.message });
  }
});

module.exports = router;
