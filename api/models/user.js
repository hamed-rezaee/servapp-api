const Joi = require("joi");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const Constants = require("../helpers/constants");

const schema = mongoose.Schema({
  username: { type: String, unique: true, maxlength: 32, required: true },
  phoneNo: { type: String, unique: true, maxlength: 16, required: true },
  password: { type: String, maxlength: 128, required: true },
  code: { type: String, maxlength: 16, required: true },
  presenter: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: false },
  profile: { type: mongoose.Schema.Types.ObjectId, ref: "Profile", required: false },
  activationCode: { type: String, maxlength: 16, required: false },
  token: { type: String, required: false },
  active: { type: Boolean, default: false, required: false },
  deleted: { type: Boolean, default: false, required: false }
});

schema.methods.generateAuthToken = async function() {
  return jwt.sign({ userId: this._id }, Constants.JWT_PRIVATE_KEY, { expiresIn: "28d" });
};

const Model = mongoose.model("User", schema);

function validateRgister(request) {
  const schema = {
    username: Joi.string().required(),
    phoneNo: Joi.string().required(),
    password: Joi.string().required(),
    presenterCode: Joi.string()
      .optional()
      .allow("")
  };

  return Joi.validate(request, schema);
}

function validateLogin(request) {
  const schema = {
    username: Joi.string().required(),
    password: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

function validateActivationCode(request) {
  const schema = {
    activationCode: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

function validatePhoneNo(request) {
  const schema = {
    phoneNo: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

module.exports.User = Model;
module.exports.validateRgister = validateRgister;
module.exports.validateLogin = validateLogin;
module.exports.validateActivationCode = validateActivationCode;
module.exports.validatePhoneNo = validatePhoneNo;
