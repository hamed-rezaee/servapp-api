const Joi = require("joi");
const mongoose = require("mongoose");

const schema = mongoose.Schema({
  fileName: { type: String, required: true },
  contentType: { type: String, required: true },
  content: { type: Buffer, required: true },
  uploadDate: { type: Date, required: true },
  deleted: { type: Boolean, default: false, require: false }
});

const Model = mongoose.model("File", schema);

function validateDownloadRequest(request) {
  const schema = {
    _id: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

module.exports.File = Model;
module.exports.validateDownloadRequest = validateDownloadRequest;
