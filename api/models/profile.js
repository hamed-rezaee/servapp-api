const Joi = require("joi");
const mongoose = require("mongoose");

const schema = mongoose.Schema({
  firstName: { type: String, maxlength: 32, required: true },
  lastName: { type: String, maxlength: 32, required: true },
  nationalCode: { type: String, maxlength: 16, required: true },
  score: { type: Number, default: 0, required: false },
  avatar: { type: mongoose.Schema.Types.ObjectId, ref: "File", required: false },
  addresses: [{ type: mongoose.Schema.Types.ObjectId, ref: "Address", required: false }],
  deleted: { type: Boolean, default: false, required: false }
});

const Model = mongoose.model("Profile", schema);

function validateSave(request) {
  const schema = {
    _id: Joi.string().optional(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    nationalCode: Joi.string().required(),
    score: Joi.number().optional(),
    userId: Joi.string().required(),
    avatarId: Joi.string()
      .optional()
      .allow("")
  };

  return Joi.validate(request, schema);
}

module.exports.Profile = Model;
module.exports.validateSave = validateSave;
