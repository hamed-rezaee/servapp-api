const Joi = require("joi");
const mongoose = require("mongoose");

const schema = mongoose.Schema({
  title: { type: String, maxlength: 32, required: true },
  detail: { type: String, maxlength: 256, required: true },
  range: { type: String, maxlength: 256, required: false },
  latitude: { type: Number, default: 0, required: true },
  longitude: { type: Number, default: 0, required: true },
  profile: { type: mongoose.Schema.Types.ObjectId, ref: "Profile", required: true },
  staticMap: { type: mongoose.Schema.Types.ObjectId, ref: "File", required: true },
  deleted: { type: Boolean, default: false, require: false }
});

const Model = mongoose.model("Address", schema);

function validateSave(request) {
  const schema = {
    _id: Joi.string().optional(),
    title: Joi.string().required(),
    detail: Joi.string().required(),
    range: Joi.string()
      .optional()
      .allow(""),
    latitude: Joi.number().required(),
    longitude: Joi.number().required(),
    profileId: Joi.string().required(),
    staticMapId: Joi.string().required()
  };

  return Joi.validate(request, schema);
}

module.exports.Address = Model;
module.exports.validateSave = validateSave;
